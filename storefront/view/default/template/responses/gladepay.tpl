


<form method="post" action="<?php echo $form_callback; ?>" id="checkout_form">
    <input type="hidden" name="inline_js" value="<?php echo $inline_js; ?>"/>
    <input type="hidden" name="mid" value="<?php echo $mid; ?>"/>
    <input type="hidden" name="amount" value="<?php echo $amount; ?>"/>
    <input type="hidden" name="currency" value="<?php echo $currency; ?>"/>
    <input type="hidden" name="id" value="<?php echo $id; ?>"/>
    <input type="hidden" name="response" value="" id="response"/>
</form>

<div class="form-group action-buttons text-center">
    <a id="<?php echo $back->name ?>" href="<?php echo $back->href; ?>" class="btn btn-default mr10" title="<?php echo $back->text ?>">
        <i class="fa fa-arrow-left"></i>
        <?php echo $back->text ?>
    </a>
    <a href="javascript:pay()">
        <button id="btn-checkout" class="btn btn-orange lock-on-click" title="<?php echo $button_confirm->name ?>" type="submit">
            <i class="fa fa-check"></i>
            <?php echo $button_confirm->name; ?>
        </button>
    </a>
</div>

<script type="text/javascript" src="<?php echo $inline_js ?>"></script>

<script>
    function pay(){
        initPayment({
                    MID:"<?php echo $mid; ?>",
                    email: "<?php echo $email; ?>",
                    amount: "<?php echo $amount; ?>",
                    country: "<?php echo $country; ?>",
                    currency: "<?php echo $currency; ?>",
                    onclose: function() {
                    },
                    callback: function(response) {
                        console.log("Payment successful");
                        console.log(response);

                        //add response to hidden form
                        document.getElementById('response').value = JSON.stringify(response);

                        //submit the checkout form for processing
                        var gladepayResponse = document.getElementById('checkout_form')
                        gladepayResponse.submit();
                    }
                    
                });
    }
</script>