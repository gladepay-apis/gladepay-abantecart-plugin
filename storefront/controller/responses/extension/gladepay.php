<?php

if ( !defined ( 'DIR_CORE' )) {
    header ( 'Location: static_pages/' );
}

class ControllerResponsesExtensionGladepay extends AController{

    public $data = array();
    
    public function main(){

        $this->data['button_confirm'] = $this->language->get('button_confirm');
        $this->data['button_back'] = $this->language->get('button_back');

        //these are passed to checkout form in gladepay.tpl
        if ($this->config->get('gladepay_sandbox') == 'test') {
            
            $this->data['mid'] = $this->config->get('gladepay_tmid');
            $this->data['inline_js'] = "http://demo.api.gladepay.com/checkout.js"; //inline checkout endpoint
        }
        else {
            $this->data['mid'] = $this->config->get('gladepay_lmid'); //live merchant_ID
            $this->data['inline_js'] = "http://api.gladepay.com/checkout.js"; //inline checkout endpoint
        }

        if($this->config->get('embed_mode')) {
            $this->data['target_parent'] = 'target="_parent"';
        }

        $this->load->model('checkout/order');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $this->data['amount'] = $this->currency->format($order_info['total'], $order_info['currency'], $order_info['value'], FALSE);
        $this->data['country'] = $order_info['shipping_iso_code_2'];
        $this->data['currency'] = $order_info['currency'];
        $this->data['phone'] = $order_info['telephone'];
        $this->data['email'] = $order_info['email'];
        $this->data['city'] = $order_info['shipping_city'];
        $this->data['postal_code'] = $order_info['shipping_postcode'];
        $this->data['address'] = $order_info['shipping_address_1'];
        $this->data['form_callback'] = $this->html->getSecureURL('extension/gladepay/callback');

        $this->load->library('encryption');
        $encryption = new AEncryption($this->config->get('encryption_key'));
        $this->data['id'] = $encryption->encrypt($this->session->data['order_id']);
        
        if ($this->request->get['rt'] != 'checkout/guest_step_3') {
            $this->data['back'] = $this->html->getSecureURL('checkout/payment');
        } else {
            $this->data['back'] = $this->html->getSecureURL('checkout/guest_step_2');
        }

        $back = $this->request->get[ 'rt' ] != 'checkout/guest_step_3'
            ? $this->html->getSecureURL('checkout/payment')
            : $this->html->getSecureURL('checkout/guest_step_2');

        $this->data[ 'back' ] = HtmlElementFactory::create(array( 'type' => 'button',
            'name' => 'back',
            'text' => $this->language->get('button_back'),
            'style' => 'button',
            'href' => $back ));

        $this->data[ 'button_confirm' ] = HtmlElementFactory::create(
            array( 'type' => 'submit',
                'name' => $this->language->get('button_confirm'),
                'style' => 'button',
            ));

        $this->view->batchAssign( $this->data );
        $this->processTemplate('responses/gladepay.tpl');
    }

    function callback(){

        include_once 'verify.php';

        if ($this->request->is_GET()) {
            $this->redirect($this->html->getURL('index/home'));
        }

        $this->load->library('encryption');
        $encryption = new AEncryption($this->config->get('encryption_key'));

        if (isset($this->request->post['id'])) {
            $order_id = $encryption->decrypt($this->request->post['id']);
        } else {
            $order_id = 0;
        }

        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        if(!$order_info){
            error_log('asd',0);
            return null;
        }
        if ($this->config->get('gladepay_sandbox') == 'test') {
            //test credentials needed during verification in verify.php verify_txn()
            $mid =  $this->config->get('gladepay_tmid');
            $mkey = $this->config->get('gladepay_tmkey');
            $verification_endpoint = "https://demo.api.gladepay.com/payment";
        }
        else {
            //live credentials needed during verification in verify.php verify_txn()
            $mid =  $this->config->get('gladepay_tmid');
            $mkey = $this->config->get('gladepay_tmkey');
            $verification_endpoint = "https://api.gladepay.com/payment";
        } 

        //Response from gladepay received from gladepay.tpl js 
        //html_entity_decode() removes browser-added html characters like $quote. Adding ,true makes it an array from an object
        $gladepayResponse = json_decode(html_entity_decode($_POST['response']),true);
        

        $gladepayRef = $gladepayResponse['txnRef']; 
        $verification = verify_txn($gladepayRef, $mid, $mkey, $verification_endpoint); //make sure you pass endpoint, mid, key and txnref


        //convert verification response from gladepay to an object. Used verify complete transaction.   
        $verification = json_decode($verification);
        

        //These conditions should be custom to gladepay responses. USE txnStatus, and fraudStatus
        if ($verification->fraudStatus!=='ok' || $verification->txnStatus==='failed') {
            $this->model_checkout_order->confirm($order_id, $this->config->get('config_order_status_id'),'Payment Failed. Try again');

            $msg = new AMessage();
            $msg->saveError('Gladepay Payment','Error verifying the payment of the order '.$verification->txnRef);
        }else{
            //checkout message
          $this->model_checkout_order->confirm($order_id, $this->config->get('gladepay_order_status_id'),'Payment was successful, Transaction ID : '.$verification->txnRef);

        }


        //REDIRECT WHEN GLADEPAY SAYS OKAY
        $this->redirect($this->html->getURL('checkout/success'));

    }

}
