<?php

function verify_txn($gladepayRef, $mid, $mkey, $verification_endpoint){

  //It is later encoded to JSON format before being sent to gladepay verification endpoint
  $payload = [
    "action" => "verify",
    'txnRef' => $gladepayRef
];

//GLADEPAY VERIFICATION request sent via CURL. (this is for php)
$curl = curl_init();

curl_setopt_array($curl, array(
CURLOPT_URL => $verification_endpoint,
CURLOPT_RETURNTRANSFER => true,
CURLOPT_ENCODING => "",
CURLOPT_MAXREDIRS => 10,
CURLOPT_TIMEOUT => 30,
CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
CURLOPT_CUSTOMREQUEST => "PUT",
CURLOPT_POSTFIELDS => json_encode($payload),
CURLOPT_HTTPHEADER => array(
    "key: ".$mkey,
    "mid: ".$mid
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) { 
return "cURL Error #:" . $err;
} else {

//json string response received from gladepay
return $response;

}
  return true;
}
