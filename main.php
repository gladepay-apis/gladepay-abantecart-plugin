<?php

if (!defined('DIR_CORE')) {
    header('Location: static_pages/');
}

$controllers = array(
    'storefront' => array(
        'responses/extension/gladepay'
    ),
    'admin' => array( ),
);

$models = array(
    'storefront' => array( 'extension/gladepay' ),
    'admin' => array( ),
);

$languages = array(
    'storefront' => array(
        'gladepay/gladepay'),
    'admin' => array(
        'gladepay/gladepay'));

$templates = array(
    'storefront' => array(
        'responses/gladepay.tpl'),
    'admin' => array());
