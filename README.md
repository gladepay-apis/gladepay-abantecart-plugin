# GladePay AbanteCart Payment Plugin

Payment plugin for AbanteCart version 1.0.0. It works well for subsequent versions up to v1.2.13 and integrates with Gladepay Inline-Checkout

Support email: a.anthony@gladepay.com

AbanteCart is an ecommerce platform for selling of products

# Prerequisite
1. Shop owner has signed up with GladePay as a Merchant at: https://dashboard.gladepay.com/register
2. Shop owner has AbanteCart installed

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.
------------
1. Download AbanteCart from http://www.abantecart.com/download-abantecart
2. Install AbanteCart by navigating to the 'public_html' from your local server from your web browser. Follow and complete installation steps from your browser.

# Installation steps for plugin
1. Download the plugin from https://developer.gladepay.com/sdk-plugins/
2. Login to your Admin dashboard. Navigate to Extensions->Install Extension->Extension Upload->Choose the compressed folder (.tar.gz) from your computer.
![](image/1.jpg)
![](image/2.jpg)
3. Read and agree to the license terms and conditions.
![](image/3.jpg)
4. Set Configuration for test or live mode (Live credentials from https://dashboard.gladepay.com)
![](image/4.jpg)
5. Go to Extension->Payments and Enable the plugin and Save.
![](image/5.jpg)
6. Go to Dashboard->System->Localisation->Currencies and configure currency settings to Nigerian Naira.
![](image/10.jpg)
![](image/11.jpg)
6. Go to your Store and shop normally. 

# Paying with Gladepay at checkout
1. Customer enters billing information during checkout and Selects 'Gladepay' as payment method during checkout.
![](image/6.jpg)
2. Customer makes payment.
![](image/8.jpg)
![](image/9.jpg)
![](image/12.jpg)
3. Customer gets invoice.
![](image/13.jpg)
![](image/14.jpg)

## Authors
GladePay.
Contributor and Support (email): a.anthony@gladepay.com

## Acknowledgments
*AbanteCart Developer Support Forums
*Existing AbanteCart payment plugin developers and payment gateway companies